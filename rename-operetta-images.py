import string
from pathlib import Path
import argparse
import numpy as np


# Instantiate the parser
parser = argparse.ArgumentParser(
    description='Rename operetta files with appropriate well names')
parser.add_argument(
    'folder_path', default='./', nargs='?',
    help='Path to folder with images (default: current directory)')
args = parser.parse_args()

# Create well mapping dict
string.ascii_uppercase
well_mapping = {}
for row, row_letter in zip(range(1, 9), string.ascii_uppercase):
    for col in range(1, 13):
        well_mapping[f'r{row:02}c{col:02}'] = f'{row_letter}{col}'
#         print(f'r{row:02}c{col:02}', f'{row_letter}{col}')

# Create field mapping dict
mid_snake = np.array([
            [2,  3,  4,  5,  6,  7,  8,  9, 10],
            [19, 18, 17, 16, 15, 14, 13, 12, 11],
            [20, 21, 22, 23, 24, 25, 26, 27, 28],
            [37, 36, 35, 34, 33, 32, 31, 30, 29],
            [38, 39, 40, 41, 1, 42, 43, 44, 45],
            [54, 53, 52, 51, 50, 49, 48, 47, 46],
            [55, 56, 57, 58, 59, 60, 61, 62, 63],
            [72, 71, 70, 69, 68, 67, 66, 65, 64],
            [73, 74, 75, 76, 77, 78, 79, 80, 81], ])
spiral = np.array([
            [57, 58, 59, 60, 61, 62, 63, 64, 65],
            [56, 31, 32, 33, 34, 35, 36, 37, 66],
            [55, 30, 13, 14, 15, 16, 17, 38, 67],
            [54, 29, 12,  3,  4,  5, 18, 39, 68],
            [53, 28, 11,  2,  1,  6, 19, 40, 69],
            [52, 27, 10,  9,  8,  7, 20, 41, 70],
            [51, 26, 25, 24, 23, 22, 21, 42, 71],
            [50, 49, 48, 47, 46, 45, 44, 43, 72],
            [81, 80, 79, 78, 77, 76, 75, 74, 73], ])
field_mapping = {}
for mid_snake_value in range(mid_snake.min(), mid_snake.max() + 1):
    spiral_value = spiral[np.where(mid_snake == mid_snake_value)]
    field_mapping[mid_snake_value] = spiral_value[0]

# Rename files
for fname in Path(args.folder_path).glob('r??c??f??p??*.tiff'):
    well = str(fname.name)[:6]
    field = int(fname.name[7:9])
    print(field, field_mapping[field])
    new_basename = f'w{well_mapping[well]}f{field_mapping[field]}{str(fname.name)[9:]}'
    new_path = Path(args.folder_path) / Path(f'{new_basename}')
    print(f'{fname} -> {new_path}')
    fname.rename(new_path)
