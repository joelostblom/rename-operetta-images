Renaming script for converting operetta row column (`r??c??`) naming
conventions into standard well name (e.g. `A1`, `B10`, etc). The script will
use the current directory as the default path to look for images.
Alternatively, any path can be supplied as the first argument.

```
usage: operetta-renaming.py [-h] [folder_path]

Rename operetta files with appropriate well names

positional arguments:
  folder_path  Path to folder with images (default: current directory)

optional arguments:
  -h, --help   show this help message and exit
```
